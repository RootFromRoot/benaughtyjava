package com.app.benaughty.data;

import androidx.multidex.MultiDexApplication;

import com.appsflyer.AppsFlyerConversionListener;
import com.appsflyer.AppsFlyerLib;

import java.util.Map;

import timber.log.Timber;

import static com.app.benaughty.data.Consts.AF_DEV_KEY;

public class Application extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        initAppsFlayer();
    }

    private void initAppsFlayer() {
      AppsFlyerConversionListener conversionListener = new AppsFlyerConversionListener() {
            @Override
            public void onInstallConversionDataLoaded(Map<String, String> conversionData) {
            }

            @Override
            public void onInstallConversionFailure(String s) {
                Timber.e("error getting conversion data: $errorMessage");
            }

            @Override
            public void onAppOpenAttribution(Map<String, String> conversionData) {

            }

            @Override
            public void onAttributionFailure(String s) {
                Timber.e("error onAttributionFailure : $errorMessage");

            }
        };

        AppsFlyerLib.getInstance().init(AF_DEV_KEY, conversionListener, this);
        AppsFlyerLib.getInstance().startTracking(this);

    }

}
