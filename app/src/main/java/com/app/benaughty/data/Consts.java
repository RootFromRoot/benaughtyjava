package com.app.benaughty.data;

public class Consts {
    public static String AF_DEV_KEY = "cuAYNrvpxPc6ep9SFQPLyS";
    public static String LINK = "https://2youapp.work/md/testred/?did=";
    public static String VALUE_NOT_FOUND = "VALUE_NOT_FOUND";
    public static String PREF_NAME_AF_ID = "PREF_NAME_AF_ID";
    public static String PREF_NAME_SAVED_LINK = "PREF_NAME_SAVED_LINK";
    public static String DOMEN = "benaughty";
    public static String DOMEN_2 = "hotclick";
    public static String DOMEN_3 = "2youapp";
    public static String DOMEN_TEST = "google";

    public static Boolean ASWP_ONLYCAM = false;
    public static Boolean ASWP_MULFILE = true;
    public static Boolean ASWP_EXTURL = false;
    public static Boolean ASWP_CERT_VERIFICATION = true;
    public static String ASWV_URL = "https://www.benaughty.com/";
    public static String ASWV_F_TYPE = "*/*";
    public static Integer asw_file_req = 1;
}
