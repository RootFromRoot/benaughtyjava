package com.app.benaughty.data;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

public class InternetConnectionChecker {

    private Context context;
    private OnInternetConnectionListener listener;

    public InternetConnectionChecker(Context context) {
        this.context = context;
    }

    public interface OnInternetConnectionListener {
        void onConnected(boolean isConnected);
    }

    public void setListener(OnInternetConnectionListener listener) {
        this.listener = listener;
    }

    public void checkConnection() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    listener.onConnected(isConnected());
                }
            }
        });
    }

    public boolean isConnected() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
