package com.app.benaughty.data;

import android.content.Context;
import android.content.SharedPreferences;

import static com.app.benaughty.data.Consts.VALUE_NOT_FOUND;

public class PrefManager {

    private static SharedPreferences sharedPref(Context context) {
        return context.getSharedPreferences("Pref", Context.MODE_PRIVATE);
    }

    public static void saveStringToPref(Context context, String name, String value) {
        sharedPref(context).edit().putString(name, value).apply();
    }

    public static String getStringFromPref(Context context, String name) {
        return sharedPref(context).getString(name, VALUE_NOT_FOUND);
    }
}
