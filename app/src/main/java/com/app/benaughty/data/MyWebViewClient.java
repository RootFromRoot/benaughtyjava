package com.app.benaughty.data;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import static com.app.benaughty.data.Consts.DOMEN;
import static com.app.benaughty.data.Consts.DOMEN_2;
import static com.app.benaughty.data.Consts.DOMEN_3;
import static com.app.benaughty.data.Consts.DOMEN_TEST;
import static com.app.benaughty.data.Consts.PREF_NAME_SAVED_LINK;

public class MyWebViewClient extends WebViewClient {

    private Context context;

    public MyWebViewClient(Context context) {
        this.context = context;
    }

    private Boolean checkIsNeededDomen(String url) {
        return url.contains(DOMEN) || url.contains(DOMEN_2) || url.contains(DOMEN_3);
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
        String url = request.getUrl().toString();
        if (checkIsNeededDomen(url))
            view.loadUrl(url);
        else
            openUrlInBrowser(url);
        return true;
    }

    private void openUrlInBrowser(String url) {
        String uri = url;
        if (!url.startsWith("http://") && !url.startsWith("https://"))
            uri = "http://$url";
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        context.startActivity(browserIntent);
    }


    @Override
    public void doUpdateVisitedHistory(WebView view, String url, boolean isReload) {
        super.doUpdateVisitedHistory(view, url, isReload);
        if (checkIsNeededDomen(url)) {
            PrefManager.saveStringToPref(context, PREF_NAME_SAVED_LINK, url);
        }
    }
}
