
package com.app.benaughty.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.SslErrorHandler;
import android.webkit.URLUtil;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.app.benaughty.R;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.security.auth.callback.Callback;

import static android.content.Context.DOWNLOAD_SERVICE;
import static com.app.benaughty.data.Consts.ASWP_CERT_VERIFICATION;
import static com.app.benaughty.data.Consts.ASWP_EXTURL;
import static com.app.benaughty.data.Consts.ASWP_MULFILE;
import static com.app.benaughty.data.Consts.ASWP_ONLYCAM;
import static com.app.benaughty.data.Consts.ASWV_F_TYPE;
import static com.app.benaughty.data.Consts.ASWV_URL;
import static com.app.benaughty.data.Consts.asw_file_req;


public class WebViewConfigurator {
    //File Downloading
    private final static int file_perm = 2;
    public static String ASWV_HOST = aswm_host(ASWV_URL);
    static boolean ASWP_CAMUPLOAD = true;
    static boolean ASWP_FUPLOAD = true;
    String asw_cam_message;
    ValueCallback<Uri> asw_file_message;
    ValueCallback<Uri[]> asw_file_path;
    String CURR_URL = ASWV_URL;
    private Intent takePictureIntent = null;
    private JSONObject json;
    private AppCompatActivity ctx;
    private WebView webView;

    public WebViewConfigurator(AppCompatActivity ctx, WebView webView) {
        this.ctx = ctx;
        this.webView = webView;
    }

    //File Downloading
    void atachOnWebViewDownloadListener(WebView webv) {
        webv.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimeType, long contentLength) {

                if (!check_permission(2)) {
                    ActivityCompat.requestPermissions(ctx, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, file_perm);
                } else {
                    DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));

                    request.setMimeType(mimeType);
                    String cookies = CookieManager.getInstance().getCookie(url);
                    request.addRequestHeader("cookie", cookies);
                    request.addRequestHeader("User-Agent", userAgent);
                    request.setDescription("Download");
                    request.setTitle(URLUtil.guessFileName(url, contentDisposition, mimeType));
                    request.allowScanningByMediaScanner();
                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                    request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, URLUtil.guessFileName(url, contentDisposition, mimeType));
                    DownloadManager dm = (DownloadManager) ctx.getSystemService(DOWNLOAD_SERVICE);
                    assert dm != null;
                    dm.enqueue(request);
                }
            }
        });

        if (Build.VERSION.SDK_INT >= 21) {
            ctx.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            ctx.getWindow().setStatusBarColor(ctx.getResources().getColor(R.color.colorPrimaryDark));
            webv.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            webv.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        } else if (Build.VERSION.SDK_INT >= 19) {
            webv.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }
        webv.setVerticalScrollBarEnabled(false);
        webv.setWebViewClient(new Callback());

        //Rendering the default URL
        // aswm_view(ASWV_URL, false);
    }

    void atachOnWebViewClickListener(WebView webv) {
        webv.setWebChromeClient(new WebChromeClient() {
            //Handling input[type="file"] requests for android API 16+
            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                if (ASWP_FUPLOAD) {
                    asw_file_message = uploadMsg;
                    Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                    i.addCategory(Intent.CATEGORY_OPENABLE);
                    i.setType(ASWV_F_TYPE);
                    if (ASWP_MULFILE) {
                        i.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    }
                    ctx.startActivityForResult(Intent.createChooser(i, "Choose..."), asw_file_req);
                }
            }

            //Handling input[type="file"] requests for android API 21+
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {

                Log.i("MainActivity", "Checked");
                if (ASWP_FUPLOAD) {
                    if (asw_file_path != null) {
                        asw_file_path.onReceiveValue(null);
                    }
                    asw_file_path = filePathCallback;
                    Log.i("MainActivity path", asw_file_path.toString());

                    if (ASWP_CAMUPLOAD) {
                        takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePictureIntent.resolveActivity(ctx.getPackageManager()) != null) {
                            File photoFile = null;
                            try {
                                photoFile = createImage();
                                takePictureIntent.putExtra("PhotoPath", asw_cam_message);
                            } catch (Exception ex) {
                                Log.e("MainActivity", "Image file creation failed", ex);
                            }
                            if (photoFile != null) {
                                asw_cam_message = "file:" + photoFile.getAbsolutePath();
                                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                            } else {
                                takePictureIntent = null;
                            }
                        }
                    }
                    Intent contentSelectionIntent = new Intent(Intent.ACTION_GET_CONTENT);
                    if (!ASWP_ONLYCAM) {
                        contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE);
                        contentSelectionIntent.setType(ASWV_F_TYPE);
                        if (ASWP_MULFILE) {
                            contentSelectionIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        }
                    }
                    Intent[] intentArray;
                    if (takePictureIntent != null) {
                        intentArray = new Intent[]{takePictureIntent};
                    } else {
                        intentArray = new Intent[0];
                    }

                    Intent chooserIntent = new Intent(Intent.ACTION_CHOOSER);
                    chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent);
                    chooserIntent.putExtra(Intent.EXTRA_TITLE, "Choose...");
                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
                    ctx.startActivityForResult(chooserIntent, asw_file_req);
                }
                return true;
            }

            @Override
            public void onProgressChanged(WebView view, int p) {
            }

            private File createImage() throws IOException {
                @SuppressLint("SimpleDateFormat")
                String file_name = new SimpleDateFormat("yyyy_mm_ss").format(new Date());
                String new_name = "file_" + file_name + "_";
                File sd_directory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                return File.createTempFile(new_name, ".jpg", sd_directory);
            }

        });
    }

    public void checkPermissions() {
        String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

        //Checking for storage permission to write images for upload
        if (ASWP_FUPLOAD && ASWP_CAMUPLOAD && !check_permission(2) && !check_permission(3)) {
            ActivityCompat.requestPermissions(ctx, perms, file_perm);

            //Checking for WRITE_EXTERNAL_STORAGE permission
        } else if (ASWP_FUPLOAD && !check_permission(2)) {
            ActivityCompat.requestPermissions(ctx, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, file_perm);

            //Checking for CAMERA permissions
        } else if (ASWP_CAMUPLOAD && !check_permission(3)) {
            ActivityCompat.requestPermissions(ctx, new String[]{Manifest.permission.CAMERA}, file_perm);
        }
    }

    public boolean check_permission(int permission) {
        switch (permission) {
            case 2:
                Log.i("MainActivity", "Permission 2");
                return ContextCompat.checkSelfPermission(ctx, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
            case 3:
                Log.i("MainActivity", "Permission 3");
                return ContextCompat.checkSelfPermission(ctx, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
        }
        return false;
    }

    void aswm_view(String url, Boolean tab) {
        if (tab) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            ctx.startActivity(intent);
        } else {
            if (url.contains("?")) { // check to see whether the url already has query parameters and handle appropriately.
                url += "&";
            } else {
                url += "?";
            }
            url += "rid=" + random_id();
            webView.loadUrl(url);
        }
    }

    public String random_id() {
        return new BigInteger(130, new SecureRandom()).toString(32);
    }

    public boolean url_actions(WebView view, String url) {
        boolean a = true;

        //Opening external URLs in android default web browser
        if (ASWP_EXTURL && !aswm_host(url).equals(ASWV_HOST)) {
            aswm_view(url, true);
        } else {
            a = false;
        }
        return a;
    }

    public static String aswm_host(String url) {
        if (url == null || url.length() == 0) {
            return "";
        }
        int dslash = url.indexOf("//");
        if (dslash == -1) {
            dslash = 0;
        } else {
            dslash += 2;
        }
        int end = url.indexOf('/', dslash);
        end = end >= 0 ? end : url.length();
        int port = url.indexOf(':', dslash);
        end = (port > 0 && port < end) ? port : end;
        Log.w("URL Host: ", url.substring(dslash, end));
        return url.substring(dslash, end);
    }
    //End File Downloading

    private class Callback extends WebViewClient {

        //For android below API 23
        @SuppressWarnings("deprecation")
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            Toast.makeText(ctx, "Error", Toast.LENGTH_SHORT).show();
        }

        //Overriding webview URLs
        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            CURR_URL = url;
            return url_actions(view, url);
        }

        //Overriding webview URLs for API 23+ [suggested by github.com/JakePou]
        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            CURR_URL = request.getUrl().toString();
            return url_actions(view, request.getUrl().toString());
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            if (ASWP_CERT_VERIFICATION) {
                super.onReceivedSslError(view, handler, error);
            } else {
                handler.proceed(); // Ignore SSL certificate errors
            }
        }
    }

}
