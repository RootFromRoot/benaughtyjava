package com.app.benaughty.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.app.benaughty.R;
import com.app.benaughty.data.InternetConnectionChecker;
import com.app.benaughty.data.MyWebViewClient;
import com.app.benaughty.data.PrefManager;
import com.appsflyer.AppsFlyerLib;

import java.util.ArrayList;
import java.util.Arrays;

import static com.app.benaughty.data.Consts.ASWP_MULFILE;
import static com.app.benaughty.data.Consts.LINK;
import static com.app.benaughty.data.Consts.PREF_NAME_AF_ID;
import static com.app.benaughty.data.Consts.PREF_NAME_SAVED_LINK;
import static com.app.benaughty.data.Consts.VALUE_NOT_FOUND;
import static com.app.benaughty.data.Consts.asw_file_req;

public class MainActivity extends AppCompatActivity {

    private WebView wv;
    private RelativeLayout wrp_no_inet;
    private Button btn_reload;
    private WebViewConfigurator webViewConfigurator;
    private InternetConnectionChecker inertChecker;
    private InternetConnectionChecker.OnInternetConnectionListener listener;

    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            if (msg.what == 1) {
                webViewGoBack();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        webViewConfigurator = new WebViewConfigurator(this, wv);
        inertChecker = new InternetConnectionChecker(this);
        inertChecker.setListener(new InternetConnectionChecker.OnInternetConnectionListener() {
            @Override
            public void onConnected(final boolean isConnected) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        if (!isConnected) {
                            wrp_no_inet.setVisibility(View.VISIBLE);
                            wv.setVisibility(View.GONE);
                        }
                    }
                });
            }
        });
        inertChecker.checkConnection();
        setupUi();
    }

    private void setupUi() {
        setupWebView();
        wv.loadUrl(getLink());
        setWebViewKeyListener();
        btn_reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (inertChecker.isConnected()) {
                    wrp_no_inet.setVisibility(View.GONE);
                    wv.loadUrl(getLink());
                    wv.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void setWebViewKeyListener() {
        wv.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK
                        && event.getAction() == MotionEvent.ACTION_UP
                        && wv.canGoBack()
                ) {
                    handler.sendEmptyMessage(1);
                    return true;
                }
                return false;
            }
        });
    }

    private void initViews() {
        wv = findViewById(R.id.wv);
        wrp_no_inet = findViewById(R.id.wrp_no_inet);
        btn_reload = findViewById(R.id.btn_reload);
    }

    private void webViewGoBack() {
        wv.goBack();
    }

    private String getLink() {
        String savedLink = PrefManager.getStringFromPref(this, PREF_NAME_SAVED_LINK);
        if (!savedLink.equals(VALUE_NOT_FOUND)) return savedLink;
        else return LINK + getAppsFlayerId(); /*"http:google.com"*/
    }

    private String getAppsFlayerId() {
        String localId = PrefManager.getStringFromPref(this, PREF_NAME_AF_ID);
        if (!localId.equals(VALUE_NOT_FOUND)) return localId;
        else {
            String id = AppsFlyerLib.getInstance().getAppsFlyerUID(this);
            PrefManager.saveStringToPref(this, PREF_NAME_AF_ID, id);
            return id;
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setupWebView() {
        MyWebViewClient client = new MyWebViewClient(this);
        webViewConfigurator.checkPermissions();
        webViewConfigurator.atachOnWebViewClickListener(wv);
        webViewConfigurator.atachOnWebViewDownloadListener(wv);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setUseWideViewPort(true);
        wv.getSettings().setLoadWithOverviewMode(true);
        wv.getSettings().setDomStorageEnabled(true);
        wv.setWebViewClient(client);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        try {
            java.util.concurrent.TimeUnit.SECONDS.sleep(4);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimary));
            ArrayList results = new ArrayList<Uri>();
            results.add(Uri.EMPTY);
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == asw_file_req) {
                    if (null == webViewConfigurator.asw_file_path) {
                        return;
                    }
                    if (intent == null || intent.getData() == null) {
                        if (webViewConfigurator.asw_cam_message != null) {
                            results.clear();
                            results.add(Uri.parse(webViewConfigurator.asw_cam_message));
                        }
                    } else {
                        String dataString = intent.getDataString();
                        if (dataString != null) {
                            results.clear();
                            results.add(Uri.parse(dataString));
                        } else {
                            if (ASWP_MULFILE) {
                                if (intent.getClipData() != null) {
                                    Integer numSelectedFiles = intent.getClipData().getItemCount();
                                    results.clear();
                                    results.add(Uri.EMPTY);
                                    for (java.lang.Integer i = 0; i < numSelectedFiles; i++) {
                                        results.set(i, intent.getClipData().getItemAt(i));
                                    }
                                }
                            }
                        }
                    }
                }
            }


            Uri[] uriArray = Arrays.copyOf(results.toArray(), results.toArray().length, Uri[].class);
            webViewConfigurator.asw_file_path.onReceiveValue(uriArray);
            webViewConfigurator.asw_file_path = null;
        } else {
            if (requestCode == asw_file_req) {
                if (null == webViewConfigurator.asw_file_message) return;
                Uri result;
                if (intent == null || resultCode != RESULT_OK) result = null;
                else result = intent.getData();
                webViewConfigurator.asw_file_message.onReceiveValue(result);
                webViewConfigurator.asw_file_message = null;
            }
        }
    }
}
